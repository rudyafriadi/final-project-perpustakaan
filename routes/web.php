<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PengarangController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PenerbitController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\PinjamController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\LaporanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/beranda', [HomeController::class, 'index'])->name('beranda');

Route::prefix('kategori')->group(function () {
    Route::get('/', [KategoriController::class, 'index'])->name('kategori');
    Route::post('/create', [KategoriController::class, 'create']);
    Route::post('/update', [KategoriController::class, 'update']);
    Route::get('/delete/{id}', [KategoriController::class, 'destroy']);
});

Route::prefix('anggota')->group(function () {
    Route::get('/', [AnggotaController::class, 'index'])->name('anggota');
    Route::post('/update', [AnggotaController::class, 'update']);
    Route::get('/delete/{id}', [AnggotaController::class, 'destroy']);
});

Route::prefix('pinjam')->group(function () {
    Route::get('/', [PinjamController::class, 'index'])->name('pinjam');
    Route::post('/create', [PinjamController::class, 'create']);
    Route::post('/update', [PinjamController::class, 'update']);
    Route::get('/delete/{id}', [PinjamController::class, 'destroy']);
});

Route::prefix('buku')->group(function () {
    Route::get('/', [BukuController::class, 'index'])->name('buku');
    Route::post('/create', [BukuController::class, 'create']);
    Route::get('/show/{id}', [BukuController::class, 'show']);
    Route::post('/update', [BukuController::class, 'update']);
    Route::get('/delete/{id}', [BukuController::class, 'destroy']);
});

Route::prefix('laporan')->group(function () {
    Route::get('/', [LaporanController::class, 'index'])->name('laporan');
});



Route::get('/pengarang', [PengarangController::class, 'index'])->name('pengarang');
Route::get('/pengarang/create', [PengarangController::class, 'create']);
Route::post('/pengarang', [PengarangController::class, 'store']);
Route::get('/pengarang/{$id}/edit', [PengarangController::class, 'edit']);
Route::get('/pengarang/{$id}/update', [PengarangController::class, 'update']);
Route::delete('/pengarang/{$id}', [PengarangController::class, 'destroy']);

Route::get('/penerbit', [PenerbitController::class, 'index']);
Route::get('/penerbit/create', [PenerbitController::class, 'create']);
Route::post('/penerbit', [PenerbitController::class, 'store']);
Route::get('/penerbit/{$id}/edit', [PenerbitController::class, 'edit']);
Route::get('/penerbit/{$id}/update', [PenerbitController::class, 'update']);
Route::delete('/penerbit/{$id}', [PenerbitController::class, 'destroy']);

// testing 
// Route::get('/master', function () {
//     return view('layouts.master');
// });
