<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam', function (Blueprint $table) {
            $table->id();
            $table->dateTime('tgl_pinjam');
            $table->dateTime('tgl_kembali');
            $table->integer('jumlah_buku');
            $table->unsignedBigInteger('anggota_id');
 
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('buku_id');
 
            $table->foreign('buku_id')->references('id')->on('buku')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam');
    }
}
