<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->integer('stok');
            $table->longText('ringkasan');
            $table->string('gambar');
            $table->unsignedBigInteger('kategori_id');
 
            $table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('penerbit_id');
 
            $table->foreign('penerbit_id')->references('id')->on('penerbit')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('pengarang_id');
 
            $table->foreign('pengarang_id')->references('id')->on('pengarang')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
