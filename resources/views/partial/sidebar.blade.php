<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="index.html"><img class="logo_icon img-responsive" src="{{asset('/pluto/images/logo/logo_icon.png')}}" alt="#" /></a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <div class="user_img"><img class="img-responsive" src="{{asset('/pluto/images/layout_img/user_img.jpg')}}" alt="#" /></div>
                <div class="user_info">
                    <h6>{{ Auth::user()->name }}</h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="list-unstyled components">
            <li>
                <a href="/beranda" aria-expanded="false"><i class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a>
             </li>
            {{-- <li><a href="widgets.html"><i class="fa fa-clock-o orange_color"></i> <span>Widgets</span></a></li> --}}
            <li>
                <a href="#transaksi" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-diamond purple_color"></i> <span>Transaksi</span></a>
                <ul class="collapse list-unstyled" id="transaksi">
                    <li><a href="/pinjam"> <span>Peminjaman Buku</span></a></li>
                    @if (Auth::user()->role == 1)
                    <li><a href="/buku"> <span>Registrasi Buku</span></a></li>
                    @endif
                    
                    {{-- <li><a href="icons.html">> <span>Icons</span></a></li>
                    <li><a href="invoice.html">> <span>Invoice</span></a></li> --}}
                </ul>
            </li>
            @if (Auth::user()->role == 1)
            <li>
                <a href="#master" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-clock-o orange_color"></i> <span>Master</span></a>
                <ul class="collapse list-unstyled" id="master">
                    <li><a href="/anggota"> <span>Anggota</span></a></li>
                    <li><a href="/kategori"> <span>Kategori Buku</span></a></li>
                    <li><a href="/penerbit"> <span>Penerbit</span></a></li>
                    <li><a href="/pengarang"> <span>Pengarang</span></a></li>
                </ul>
            </li>
            @endif

            <li>
                <a href="#laporan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-bar-chart-o orange_color"></i> <span>Laporan</span></a>
                <ul class="collapse list-unstyled" id="laporan">
                    <li><a href="/laporan"> <span>History Peminjaman Buku</span></a></li>
                </ul>
            </li>
            
    </div>
</nav>