@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Dashboard</h2>
        </div>
     </div>
  </div>
  <div class="row column1">
     <div class="col-md-6 col-lg-3">
        <div class="full counter_section margin_bottom_30">
           <div class="couter_icon">
              <div> 
                 <i class="fa fa-user yellow_color"></i>
              </div>
           </div>
           <div class="counter_no">
              <div>
                 <p class="total_no">2500</p>
                 <p class="head_couter">Jumlah Anggota</p>
              </div>
           </div>
        </div>
     </div>
     <div class="col-md-6 col-lg-3">
        <div class="full counter_section margin_bottom_30">
           <div class="couter_icon">
              <div> 
                 <i class="fa fa-clock-o blue1_color"></i>
              </div>
           </div>
           <div class="counter_no">
              <div>
                 <p class="total_no">12350</p>
                 <p class="head_couter">Jumlah Buku</p>
              </div>
           </div>
        </div>
     </div>
     <div class="col-md-6 col-lg-3">
        <div class="full counter_section margin_bottom_30">
           <div class="couter_icon">
              <div> 
                 <i class="fa fa-cloud-download green_color"></i>
              </div>
           </div>
           <div class="counter_no">
              <div>
                 <p class="total_no">1805</p>
                 <p class="head_couter">Transaski</p>
              </div>
           </div>
        </div>
     </div>
     <div class="col-md-6 col-lg-3">
        <div class="full counter_section margin_bottom_30">
           <div class="couter_icon">
              <div> 
                 <i class="fa fa-comments-o red_color"></i>
              </div>
           </div>
           <div class="counter_no">
              <div>
                 <p class="total_no">54</p>
                 <p class="head_couter">Kategori Buku</p>
              </div>
           </div>
        </div>
     </div>
  </div>
  

  <div class="row column3">
     <!-- testimonial -->
     <div class="col-md-12">
        <div class="dark_bg full margin_bottom_30">
           <div class="full graph_head">
              <div class="heading1 margin_0">
                 <h2>Buku Terbaru</h2>
              </div>
           </div>
           <div class="full graph_revenue">
              <div class="row">
                 <div class="col-md-12">
                    <div class="content testimonial">
                       <div id="testimonial_slider" class="carousel slide" data-ride="carousel">
                          <!-- Wrapper for carousel items -->
                          <div class="carousel-inner">
                             <div class="item carousel-item active">
                                <div class="img-box"><img src="{{asset('pluto/images/layout_img/user_img.jpg')}}" alt=""></div>
                                <p class="testimonial">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae..</p>
                                <p class="overview"><b>Michael Stuart</b>Seo Founder</p>
                             </div>
                             <div class="item carousel-item">
                                <div class="img-box"><img src="{{asset('pluto/images/layout_img/user_img.jpg')}}" alt=""></div>
                                <p class="testimonial">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae..</p>
                                <p class="overview"><b>Michael Stuart</b>Seo Founder</p>
                             </div>
                             <div class="item carousel-item">
                                <div class="img-box"><img src="{{asset('pluto/images/layout_img/user_img.jpg')}}" alt=""></div>
                                <p class="testimonial">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae..</p>
                                <p class="overview"><b>Michael Stuart</b>Seo Founder</p>
                             </div>
                          </div>
                          <!-- Carousel controls -->
                          <a class="carousel-control left carousel-control-prev" href="#testimonial_slider" data-slide="prev">
                          <i class="fa fa-angle-left"></i>
                          </a>
                          <a class="carousel-control right carousel-control-next" href="#testimonial_slider" data-slide="next">
                          <i class="fa fa-angle-right"></i>
                          </a>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- end testimonial -->
     
  </div>
  
</div> 
@endsection
