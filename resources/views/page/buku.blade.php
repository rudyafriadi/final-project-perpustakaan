@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Registrasi Buku</h2>
        </div>
     </div>
  </div>
  <div class="row column1">
    <div class="col-md-12">
      <div class="white_shd full margin_bottom_30">
         <div class="full graph_head">
            <div class="heading1 margin_0">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-placement="right">
                  Tambah
                </button>
            </div>
         </div>
      
         <div class="table_section padding_infor_info">
            <div class="table-responsive-sm ">
               <table class="table table-striped">
                  <thead class="thead-dark">
                     <tr>
                        <th>No</th>
                        <th>Cover</th>
                        <th>Judul Buku</th>
                        <th>Kategori</th>
                        <th>Stok</th>
                        <th>Aksi</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @foreach ($buku as $data)
                    <tr>
                      <td>{{$no}}</td>
                      <td><img height="200px" width="150px" src="{{asset('storage/cover/'.$data->gambar)}}" alt=""></td>
                      <td>{{$data->judul}}</td>
                      <td>{{$data->kategori->name}}</td>
                      <td>{{$data->stok}}</td>
                      <td>
                        <a href="/buku/show/{{$data->id}}" class="btn btn-success">
                          Detail
                        </a>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit" data-name="{{$data->name}}" data-id="{{$data->id}}" data-placement="right">
                          Edit
                        </button>
                        <a href="/kategori/delete/{{$data->id}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')">Delete</a>
                      </td>
                   </tr>
                   <?php $no++; ?>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
  </div>
  <!-- Modal Create -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Buku</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ url('buku/create') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Judul Buku</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul Buku" >
                @error('judul')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Kategori</label>
                <select class="form-control" name="kategori_id" id="">
                  @foreach ($kategori as $data)
                    <option value="{{$data->id}}">{{$data->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Penerbit</label>
                <select class="form-control" name="penerbit_id" id="penerbit_id">
                  @foreach ($penerbit as $data)
                    <option value="{{$data->id}}">{{$data->nama}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Pengarang</label>
                <select class="form-control" name="pengarang_id" id="pengarang">
                  @foreach ($pengarang as $data)
                    <option value="{{$data->id}}">{{$data->nama}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Ringkasan</label>
                <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10"></textarea>
                @error('ringkasan')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Stok</label>
                <input type="text" class="form-control" name="stok" id="stok" placeholder="Masukkan Jumlah Stok">
                @error('stok')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Cover</label>
                <input type="file" class="form-control" name="gambar" id="gambar" placeholder="Upload Cover Buku">
                @error('gambar')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                @enderror
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal Update -->
  <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ url('buku/update') }}" data-toggle="validator"
            enctype="multipart/form-data" role="form">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="hidden" id="id" name="id">
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama" required>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>
  
  
</div> 
@endsection

@push('scripts')
    
<script>
  $('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var name = button.data('name')
    var id = button.data('id') 

    var modal = $(this)
    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #id').val(id);
  })
</script>

@endpush
