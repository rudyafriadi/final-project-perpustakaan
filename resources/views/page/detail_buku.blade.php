@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Detail Buku</h2>
        </div>
     </div>
  </div>
  <div class="full price_table padding_infor_info">
    <div class="row">
       <!-- user profile section --> 
       <!-- profile image -->
       <div class="col-lg-12">
          <div class="full dis_flex center_text">
             <div class="profile_img"><img width="180" class="rounded-circle" src="{{asset('storage/cover/'.$buku->gambar)}}" alt="#" /></div>
             <div class="profile_contant">
                <div class="contact_inner">
                   <h3>{{$buku->judul}}</h3>
                   <p><strong>Kategori: </strong>{{$buku->kategori->name}}</p>
                   <p><strong>Pengarang: </strong>{{$buku->pengarang->nama}}</p>
                   <p><strong>Penerbit: </strong>{{$buku->penerbit->nama}}</p>
                   <p><strong>Ringkasan: </strong>{{$buku->ringkasan}}</p>
                   <p><strong>Stok: </strong>{{$buku->stok}}</p>
                   {{-- <ul class="list-unstyled">
                      <li><i class="fa fa-envelope-o"></i> : test@gmail.com</li>
                      <li><i class="fa fa-phone"></i> : 987 654 3210</li>
                   </ul> --}}
                </div>
                <div class="bottom_list">
                  <div class="left_rating">
                     <p class="ratings">
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star-o"></span></a>
                     </p>
                  </div>
                  <div class="right_button">
                     <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                     </i> <i class="fa fa-comments-o"></i> 
                     </button>
                     {{-- <button type="button" class="btn btn-primary btn-xs">
                     <i class="fa fa-user"> </i> View Profile
                     </button> --}}
                  </div>
               </div>
                {{-- <div class="user_progress_bar">
                   <div class="progress_bar">
                      <!-- Skill Bars -->
                      <span class="skill" style="width:85%;">Web Applications <span class="info_valume">85%</span></span>                   
                      <div class="progress skill-bar ">
                         <div class="progress-bar progress-bar-animated progress-bar-striped" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
                         </div>
                      </div>
                      
                   </div>
                </div> --}}
             </div>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="col-md-2"></div>
</div>
<!-- end row -->
</div>
 
  
  
</div> 
@endsection

@push('scripts')
    
<script>
  $('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var name = button.data('name')
    var id = button.data('id') 

    var modal = $(this)
    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #id').val(id);
  })
</script>

@endpush
