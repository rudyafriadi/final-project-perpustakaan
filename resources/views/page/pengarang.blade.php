@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Pengarang</h2>
        </div>
     </div>
  </div>
  <div class="row column1">
    <div class="col-md-12">
      <div class="white_shd full margin_bottom_30">
         <div class="full graph_head">
            <div class="heading1 margin_0">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-placement="right">
                  Tambah
                </button>
            </div>
         </div>
      
         <div class="table_section padding_infor_info">
            <div class="table-responsive-sm ">
               <table class="table table-striped">
                  <thead class="thead-dark">
                     <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>No Telepon</th>
                        <th>Aksi</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach ($pengarang as $key => $item)
                    <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$item->nama}}</td>
                      <td>{{$item->alamat}}</td>
                      <td>{{$item->no_telp}}</td>
                      <td>
                        <a href="/pengarang/{{$item->id}}" class="btn btn-warning">
                          Edit
                        </a>
                        <a href="/pengarang/delete/{{$item->id}}" class="btn btn-danger">
                          Delete
                        </a>
                      </td>
                   </tr>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Pengarang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="/pengarang" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
              @error('alamat')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
            <label for="no_telp">No Telepon</label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Masukkan No Telepon">
            @error('no_telp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>
  
  
</div> 
@endsection
