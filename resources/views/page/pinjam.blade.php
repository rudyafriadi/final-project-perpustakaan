@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Transaksi Peminjaman Buku</h2>
        </div>
     </div>
  </div>
  <div class="row column1">
    <div class="col-md-12">
      <div class="white_shd full margin_bottom_30">
        
      
         <div class="full price_table padding_infor_info">
          <div class="row">
             <!-- user profile section --> 
             <!-- profile image -->
             @foreach ($buku as $data)
             <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 profile_details margin_bottom_30">
              <div class="contact_blog">
                <h4 class="brief">{{$data->kategori->name}}</h4>
                <div class="contact_inner">
                    <div class="left">
                      <h3>{{$data->judul}}</h3>
                      <p><strong>Ringkasan: </strong>{{$data->ringkasan}}</p>
                      <p><strong>Penerbit: </strong>{{$data->penerbit->nama}}</p>
                      <p><strong>Pengarang: </strong>{{$data->pengarang->nama}}</p>
                      <p><strong>Stok: </strong>{{$data->stok}}</p>
                    </div>
                    <div class="right">
                      <div class="profile_contacts">
                          <img class="img-responsive" src="{{asset('storage/cover/'.$data->gambar)}}" alt="#" />
                      </div>
                    </div>
                    <div class="bottom_list">
                      <div class="left_rating">
                          <p class="ratings">
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star-o"></span></a>
                          </p>
                      </div>
                      <div class="right_button">
                          <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-thumbs-up">
                          </i> Like</i> 
                          </button>
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#pinjam" data-id="{{$data->id}}" data-judul="{{$data->judul}}" data-buku_id="{{$data->id}}"> 
                          <i class="fa fa-book"> </i> Pinjam
                          </button>
                      </div>
                    </div>
                </div>
              </div>
              </div>
             @endforeach
             
          </div>
       </div>
      </div>
   </div>
  </div>
  <!-- Modal Create -->
  <div class="modal fade" id="pinjam" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Form Pinjam Buku</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ url('pinjam/create') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Judul Buku</label>
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="buku_id" id="buku_id">
                <input type="hidden" value="{{ Auth::user()->id }}" name="user_id" id="user_id">
                <input type="text" class="form-control" name="judul" id="judul" disabled>
            </div>
            <div class="form-group">
                <label for="name">Tanggal Pinjam</label>
                <input type="date" class="form-control" name="tgl_pinjam" id="tgl_pinjam">
            </div>
            <div class="form-group">
                <label for="name">Tanggal Kembali</label>
                <input type="date" class="form-control" name="tgl_kembali" id="tgl_kembali">
            </div>
            <div class="form-group">
                <label for="name">Qty</label>
                <input type="text" class="form-control" name="jumlah_buku" id="jumlah_buku">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

 
  
  
</div> 
@endsection

@push('scripts')
    
<script>
  $('#pinjam').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var id = button.data('id') 
    var judul = button.data('judul')
    var buku_id = button.data('buku_id') 

    var modal = $(this)
    modal.find('.modal-body #id').val(id);
    modal.find('.modal-body #judul').val(judul);
    modal.find('.modal-body #buku_id').val(buku_id);
  })
</script>

@endpush
