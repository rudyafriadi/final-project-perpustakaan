@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row column_title">
     <div class="col-md-12">
        <div class="page_title">
           <h2>Anggota</h2>
        </div>
     </div>
  </div>
  <div class="row column1">
    <div class="col-md-12">
      <div class="white_shd full margin_bottom_30">
         <div class="full graph_head">
            <div class="heading1 margin_0">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-placement="right">
                  Tambah
                </button>
            </div>
         </div>
      
         <div class="table_section padding_infor_info">
            <div class="table-responsive-sm ">
               <table class="table table-striped">
                  <thead class="thead-dark">
                     <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>No Telp</th>
                        <th>Aksi</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @foreach ($anggota as $data)
                    <tr>
                      <td>{{$no}}</td>
                      <td>{{$data->user->name}}</td>
                      <td>{{$data->umur}}</td>
                      <td>{{$data->alamat}}</td>
                      <td>{{$data->no_telp}}</td>
                      <td>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit" data-name="{{$data->user->name}}" data-alamat="{{$data->alamat}}" data-umur="{{$data->umur}}" data-no_telp="{{$data->no_telp}}" data-id="{{$data->id}}" data-placement="right">
                          Edit
                        </button>
                        {{-- <a href="/game/delete/{{$data->id}}" class="btn btn-danger">
                          Delete
                        </a> --}}
                      </td>
                   </tr>
                   <?php $no++; ?>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
  </div>
  <!-- Modal Simpan -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="no_telp">No Telp</label>
                <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Masukkan No Telp">
                @error('no_telp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Masukkan E-Mail">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password Konfirmasi</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder=" Konfirmasi Password">
                @error('password_confirmation')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
              <label for="email">Role</label>
              <select class="form-control" name="role" id="role">
                <option value="">-- Pilih Role --</option>
                <option value="1">Admin</option>
                <option value="2">Member</option>
            </select>
              @error('role')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Modal Edit -->
  <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Anggota</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ url('anggota/update') }}/{{$data->id}}">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="hidden" id="id" name="id">
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama" disabled >
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat" >
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur" required>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="no_telp">No Telp</label>
                <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Masukkan No Telp" required>
                @error('no_telp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>
  
  
</div> 
@endsection

@push('scripts')
    
<script>
  $('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var name = button.data('name')
    var alamat = button.data('alamat')
    var umur = button.data('umur')
    var no_telp = button.data('no_telp')
    var id = button.data('id') 

    var modal = $(this)
    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #alamat').val(alamat);
    modal.find('.modal-body #umur').val(umur);
    modal.find('.modal-body #no_telp').val(no_telp);
    modal.find('.modal-body #id').val(id);
  })
</script>

@endpush