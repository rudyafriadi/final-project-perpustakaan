<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;
    protected $table = "buku";
    protected $fillable = [
        'judul','stok','ringkasan','gambar','kategori_id','penerbit_id','pengarang_id'
    ];

    public function kategori()
    {
        return $this->belongsTo("App\Models\Kategori");
    }

    public function pengarang()
    {
        return $this->belongsTo("App\Models\Pengarang");
    }

    public function penerbit()
    {
        return $this->belongsTo("App\Models\Penerbit");
    }

    public function pinjam()
    {
        return $this->hasMany("App\Models\Pinjam");
    }
}
