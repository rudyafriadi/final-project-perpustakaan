<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    use HasFactory;
    protected $table = "pinjam";
    protected $fillable = [
        'tgl_pinjam','tgl_kembali','jumlah_buku','user_id','buku_id'
    ];

    public function buku()
    {
        return $this->belongsTo("App\Models\Buku");
    }
}
