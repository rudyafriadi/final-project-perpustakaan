<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengarang;
use Alert;


class PengarangController extends Controller
{
    public function index()
    {
        $pengarang = Pengarang::all();

        // dd($pengarang);

        return view('page.pengarang', compact('pengarang'));
    }

    public function create()
    {
        return view('landing.tambahpengarang');
    }


    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'alamat' => 'required',
                'no_telp' => 'required'
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!',
                'alamat.required' => 'Alamat tidak boleh kosong!',
                'no_telp.required' => 'No Telepon tidak boleh kosong!',

            ]
        );

        $pengarang = Pengarang::create([
            'nama' => $request['nama'],
            'alamat' => $request['alamat'],
            'no_telp' => $request['no_telp']
        ]);

        // DB::table('pengarang')->insert([
        //     'nama' => $request['nama'],
        //     'alamat' => $request['alamat'],
        //     'no_telp' => $request['telepon']
        // ]);

        Alert::success('Selamat', 'Data berhasil disimpan');
        return redirect()->route('pengarang');
    }
};
