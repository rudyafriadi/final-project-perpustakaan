<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pinjam;
use App\Models\Buku;
use App\Models\Kategori;
use App\Models\Pengarang;
use App\Models\Penerbit;
use Alert;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        $kategori = Kategori::all();
        $penerbit = Penerbit::all();
        $pengarang = Pengarang::all();
        return view('page.pinjam', compact('buku','kategori','penerbit','pengarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        $buku = Buku::findOrFail($r->id);

        $sisastok = $buku->stok - $r->jumlah_buku;
        $buku->stok = $sisastok ;
        $buku->update();

        Pinjam::create([
            'tgl_pinjam' => $r['tgl_pinjam'],
            'tgl_kembali' => $r['tgl_kembali'],
            'jumlah_buku' => $r['jumlah_buku'],
            'buku_id' => $r['buku_id'],
            'user_id' => $r['user_id'],
        ]);

        Alert::success('Selamat', 'Data berhasil diupdate');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
