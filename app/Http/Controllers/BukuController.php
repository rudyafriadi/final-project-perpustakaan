<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use App\Models\Buku;
use App\Models\Kategori;
use App\Models\Pengarang;
use App\Models\Penerbit;
use Alert;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        $kategori = Kategori::all();
        $penerbit = Penerbit::all();
        $pengarang = Pengarang::all();
        return view('page.buku', compact('buku','kategori','penerbit','pengarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        
        $this->validate($r,[
            'judul'=> 'required',
            'stok'=> 'required',
            'ringkasan'=> 'required',
            'kategori_id'=> 'required',
            'penerbit_id'=> 'required',
            'pengarang_id'=> 'required',
            // 'gambar'=> 'required|mimes:jpeg,jpg,png,gif|max:2048'
        ]);

        $buku = new Buku;
        $buku->judul = $r->input('judul');
        $buku->stok = $r->input('stok');
        $buku->ringkasan = $r->input('ringkasan');
        $buku->kategori_id = $r->input('kategori_id');
        $buku->penerbit_id = $r->input('penerbit_id');
        $buku->pengarang_id = $r->input('pengarang_id');
        // $buku->gambar = $r->input('gambar');

        if ($r->hasfile('gambar')){
            $file = $r->file('gambar');
             $extension = $file->getClientOriginalExtension();
             $filename = $file->getClientOriginalName();
             $file->move(public_path('storage/cover/'), $filename);
             $buku->gambar = $filename;
         }

        $buku->save();
        // $buku = Buku::create([
        //     'judul' => $r['judul'],
        //     'stok' => $r['stok'],
        //     'ringkasan' => $r['ringkasan'],
        //     'kategori_id' => $r['kategori_id'],
        //     'penerbit_id' => $r['penerbit_id'],
        //     'pengarang_id' => $r['pengarang_id'],
        //     'gambar' => $gambar,
        // ]);

        
        Alert::success('Selamat', 'Data berhasil disimpan');
        return redirect()->route('buku');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::where('id', $id)->first();
        return view('page.detail_buku', compact('buku'));
        // dd($buku);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
