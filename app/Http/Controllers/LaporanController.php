<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pinjam;
use Illuminate\Support\Facades\Auth;


class LaporanController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $histori = Pinjam::where('user_id', $id)->get();
        return view('page.laporan', compact('histori'));
    }
}
